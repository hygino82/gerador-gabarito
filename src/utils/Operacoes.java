package utils;

public class Operacoes {
	public static int[][] Soma(int[][] A, int[][] B) {
		if ((A.length == B.length) && (A[0].length == B[0].length)) {
			int linhas = A.length;
			int colunas = B[0].length;
			int[][] C = new int[linhas][colunas];
			for (int i = 0; i < linhas; i++) {
				for (int j = 0; j < colunas; j++) {
					C[i][j] = A[i][j] + B[i][j];
				}
			}
			return C;
		} else {
			throw new IllegalArgumentException("Não se pode somar as matrizes");
		}
	}

	public static String imprime(int[][] a) {
		StringBuilder sb = new StringBuilder();
		int linhas = a.length;
		int colunas = a[0].length;
		for (int i = 0; i < linhas; i++) {

			for (int j = 0; j < colunas; j++) {
				sb.append(String.format("%d  ", a[i][j]));
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	public static int[][] produtoEscalar(int t, int[][] a) {
		int linhas = a.length;
		int colunas = a[0].length;
		int prod[][] = new int[linhas][colunas];

		for (int i = 0; i < linhas; i++) {
			for (int j = 0; j < colunas; j++) {
				prod[i][j] = t * a[i][j];
			}
		}

		return prod;
	}

	public static int[][] transposta(int[][] a) {
		int linhas = a.length;
		int colunas = a[0].length;
		int transp[][] = new int[colunas][linhas];

		for (int i = 0; i < linhas; i++) {
			for (int j = 0; j < colunas; j++) {
				transp[j][i] = a[i][j];
			}
		}

		return transp;
	}
}
