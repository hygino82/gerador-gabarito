package turmas;

public class TerceiroTancredo {
    private int t;

    public TerceiroTancredo(int t) {
        this.t = t;
    }

    private String questao2() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nQuestão 2\n");

        int ladoMaior = 10 + t;
        int ladoMenor = 5 + t;
        int areaMaior = ladoMaior * ladoMaior;
        int areaMenor = ladoMenor * ladoMenor;
        int areaTotal = areaMaior + areaMenor;
        sb.append(String.format("Lado maior = 10 + %d = %d\n", t, ladoMaior));
        sb.append(String.format("Área maior =  %d\n", areaMaior));

        sb.append(String.format("Lado menor = 5 + %d = %d\n", t, ladoMenor));
        sb.append(String.format("Área menor = %d\n", areaMenor));

        sb.append(String.format("Área total = %d + %d = %d\n", areaMaior, areaMenor, areaTotal));

        return sb.toString();
    }

    private String questao3() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nQuestão 3\n");
        int comprimento = 5 * t;
        int largura = 3 + t;
        int altura = 10 + t;
        int a1 = comprimento * largura;
        int a2 = comprimento * altura;
        int a3 = altura * largura;
        int areaTotal = 2 * (a1 + a2 + a3);
        int volume = comprimento * largura * altura;

        sb.append(String.format("Comprimento =  %d\n", comprimento));
        sb.append(String.format("Largura =  %d\n", largura));
        sb.append(String.format("Altura =  %d\n", altura));
        sb.append(String.format("Comprimento x Largura =  %d\n", a1));
        sb.append(String.format("Altura x Comprimento=  %d\n", a2));
        sb.append(String.format("Altura x Largura =  %d\n", a3));
        sb.append(String.format("Área total =  %d\n", areaTotal));
        sb.append(String.format("Volume =  %d\n", volume));

        return sb.toString();
    }

    private String questao4() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nQuestão 4\n");
        sb.append("\nLosango\n");
        int diagonalMaior = 12 + t;
        int diagonalMenor = 7 + t;
        double areaLosango = diagonalMaior * diagonalMenor / 2.0;
        sb.append(String.format("Diagonal Maior = 12 + %d = %d\n", t, diagonalMaior));
        sb.append(String.format("Diagonal Menor = 7 + %d = %d\n", t, diagonalMenor));
        sb.append(String.format("Área do losango = %.2f\n", areaLosango));

        sb.append("\nHexágono regular\n");
        int lado = 10 + t;
        double areaHexagono = 3 * lado * lado * 1.73 / 2.0;
        sb.append(String.format("Lado = 10 + %d = %d\n", t, lado));
        sb.append(String.format("Área do hexágono = %.2f\n", areaHexagono));
        return sb.toString();
    }

    private String questao5() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nQuestão 5\n");

        int arestaBase = 10 + t;
        int altura = 20 + t;
        double areaBase = arestaBase * arestaBase * 1.73 / 4.0;
        double areaLateral = 3 * arestaBase * altura;
        double areaTotal = 2 * areaBase + areaLateral;

        sb.append(String.format("Aresta da base = 10 + %d = %d\n", t, arestaBase));
        sb.append(String.format("Altura = 20 + %d = %d\n", t, altura));
        sb.append(String.format("Área da base = %.2f\n", areaBase));
        sb.append(String.format("Área lateral = %.2f\n", areaLateral));
        sb.append(String.format("Área total = %.2f\n", areaTotal));

        return sb.toString();
    }

    public String prova1() {
        StringBuilder sb = new StringBuilder();
        sb.append(questao2());
        sb.append(questao3());
        sb.append(questao4());
        sb.append(questao5());

        return sb.toString();
    }
}
