package turmas;

import utils.Operacoes;

public class PrimeiroAnoRec {
    private int t;

    public PrimeiroAnoRec(int t) {
        this.t = t;
    }

    private String questao1() {
        StringBuilder sb = new StringBuilder();
        sb.append("Questão 1\n");
        sb.append(String.format("C_13 = 7 * 1 + %d * %d + 8 * (%d)\n", t, t, -t));
        sb.append(String.format("C_13 = 7 + %d - %d\n", t * t, 8 * t));
        sb.append(String.format("C_13 = %d\n\n", (7 + t * t - 8 * t)));
        return sb.toString();
    }

    private String questao3() {
        StringBuilder sb = new StringBuilder();
        sb.append("Questão 3\n");
        sb.append(String.format("A = [[8\t%d], [7\t%d]]\n", -t, t));
        sb.append(String.format("B = [[4\t%d], [%d\t1]]\n", 40 - t, t));
        int a[][] = { { 8, -t }, { 7, t } };
        int b[][] = { { 4, 40 - t }, { t, 1 } };
        int c[][] = { { 4, 5, 1 }, { 2, -1, 8 }, { 7, 6, 3 } };

        sb.append("Letra A\n");
        int soma[][] = Operacoes.Soma(a, b);
        sb.append(Operacoes.imprime(soma));
        sb.append("\n");

        sb.append("Letra B\n");
        int prodA[][] = Operacoes.produtoEscalar(3, a);
        int prodB[][] = Operacoes.produtoEscalar(-2, b);
        sb.append("Matriz 3A\n");
        sb.append(Operacoes.imprime(prodA));
        sb.append("Matriz -2B\n");
        sb.append(Operacoes.imprime(prodB));
        int resDif[][] = Operacoes.Soma(prodA, prodB);
        sb.append("Matriz Resultante\n");
        sb.append(Operacoes.imprime(resDif));

        sb.append("\nLetra C\n");
        sb.append("Produto escalar\n");
        int[][] prod = Operacoes.produtoEscalar(t, c);
        sb.append(Operacoes.imprime(prod));

        sb.append("\nLetra D\n");
        int[][] transp = Operacoes.transposta(a);
        sb.append("Matriz Transposta\n");
        sb.append(Operacoes.imprime(transp));
        
        return sb.toString();
    }

    private String questao4() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nQuestão 4\n");

        for (int i = 1; i < 3; i++) {
            for (int j = 1; j < 3; j++) {
                sb.append(String.format("a_%d,%d = 5 * %d + %d * %d = %d + %d = %d\n", i, j, i, t, j, 5 * i, t * j,
                        5 * i + t * j));
            }
        }

        return sb.toString();
    }

    public String geraValores() {
        StringBuilder sb = new StringBuilder();
        sb.append(questao1());
        sb.append(questao3());
        sb.append(questao4());
        return sb.toString();
    }

}
