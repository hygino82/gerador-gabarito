package gui;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextArea;
import turmas.PrimeiroAnoRec;
import turmas.TerceiroTancredo;

public class ViewController implements Initializable {
	private String[] listaTds = { "ADRIELY FORLIN DA SILVA", "ADRYEL GUSTAVO FERREIRA SIEBRE", "ARIEL LEITE MONCHAK",
			"CLAYTSON HENRIQUE SOUZA", "CRISLAINE QUADROS DE LIMA", "EMELLY KELLN", "ERICK FIGUR DA SILVA",
			"FELIPE ALVES DE LIMA", "FELIPE COMUNELLO", "FELIPE RUFATTO DE CEZARO", "FLAVIA CRISTINA RAMALHO BORDIN",
			"GABRIEL FERREIRA", "GABRIEL MONTEIRO", "GABRIEL PIASSA RUFATO", "GUILHERME DALLA COSTA DE MORAIS",
			"GUILHERMO STRAPAZZON HOLTZ", "ISAURA BORGES DA SILVA", "ISRAEL ANTUNES DOS SANTOS",
			"KAUAN LOTTI DE ALMEIDA", "LUIZ FELIPE BELO SOARES", "LUIZ HENRIQUE DO CARMO VANELI",
			"MIGUEL BARRETO DE CRISTO", "NATALIA DE LIMA POLESE", "NATÁLIA GURAL DOS SANTOS",
			"OTÁVIO ANTONIO GALVÃO SILVA", "PAULO HENRIQUE PENA PAIM", "PEDRO HENRIQUE DE LARA SANTOS",
			"RAFAEL DE JESUS DIAS", "RAISSA DOS SANTOS", "RAÍ VINUTO LIMA OLIVEIRA", "RAUL CESAR FERNANDES BIESEKI",
			"THALIA DE LIMA TOGNION", "VICTOR ENRIQUE COSTA", "VICTOR JOSÉ ENGELMANN",
			"VICTOR KAIKE VIEIRA DE VASCONCELOS", "VICTOR RAFAEL FABIAN DOS SANTOS" };

	String[] listaNem = { "ADRIELI SILVEIRA DE ABREU", "AFONSO FRIGO DE ANDRADE", "AMANDA GILIOLI MARCOMIN",
			"ANA CLARA TRE", "ARIEL EDUARDO ROSSETTO ZAMARCHI", "ARTHUR FARIAS PROVENSI", "BRENDA LOPES",
			"BRENDON DE CAMPOS LAZARIN", "CARLOS DANIEL MEDEIROS KELM", "DARA EVELYN ETER DOS SANTOS",
			"ELIZA SANTOS MACIEL", "ELOIZA SANTOS MACIEL", "EMILI GREGOLIN", "FERNANDA SUELEN DE FRANÇA",
			"GLAUCO MARINÉ ALACARINI", "GUSTAVO CLEMENTE", "GUSTAVO HENRIQUE REZENDE",
			"ILSON HENRIQUE DE OLIVEIRA BELO", "ISABELLY VITÓRIA DE LARA SANTOS", "IZABELY MILENA TRINDADE ETER",
			"JOAO VICTOR MACHADO DE SA", "JOSÉ EDUARDO DE SOUZA LOPES", "KEVLIN SILVA LIVIZ", "LARISSA BATISTA",
			"MARIA EDUARDA ANTUNES", "MARIA EDUARDA DE SIQUEIRA", "MARIANA SANTOS DO NASCIMENTO",
			"NICOLY DE QUEIROZ PADILHA", "PABLO KLASSMANN PINHEIRO", "SAIONARA VITORIA DOS SANTOS",
			"SIDINEY RUAN POLEZ FERREIRA", "VAGNER MARTINS DE ALENCAR", "VICTOR DE PAULA RUFATTO",
			"VITOR MATEUS RODRIGUES", "WILLIAM DANIEL DOS SANTOS", "ELUANE EMANUELLE RIBEIRO PIANO",
			"EVELYN TEREZINHA DO NASCIMENTO", "CARLOS GABRIEL SILVA LOURENÇO", "GIOVANNA DALMOLIN SMANIOTO",
			"MATHEUS HENRIQUE ANDREIS DE CAMPOS", "MAIANE DE BAIRROS", "GUILHERME TROMBETTA BARRILI",
			"CHRISTIAN MIGUEL DE CAMARGO", "CRISTIAN BONFANTE DE FREITAS" };

	String[] listaTancredo = { "AGATHA MARCANTE",
			"ALISSON CARDOSO DE OLIVEIRA",
			"BARBARA VOLF FERNANDES",
			"BRUNA EDUARDA DOS SANTOS",
			"CARLOS EDUARDO DOS SANTOS RONCALIO",
			"CELSO KAUE FAGUNDES",
			"ERICKE RICARDO RONCEM SWARTZ",
			"ERICK HENRIQUE DE ALMEIDA",
			"GUSTAVO MENEGAT",
			"IAM SAMUEL DE OLIVEIRA",
			"JOCELENE FOGUESATTO",
			"KAILANY XANDRYELI FERREIRA GUIMARAES",
			"TAIANE VITORIA FRANKE DA FONSECA",
			"WELLEM NATALI PIROLA DE LIMA",
			"ELYABE MISAEL VOLFF GUCHERT",
			"WEVERTON TOMASSEVSKI",
			"CARLOS DANIEL MACHADO",
			"ELEANDRO DA SILVA",
			"PAMELA DO PRADO ALVES",
			"AMANDA KARINE GUSTMANN",
			"MATHEUS HENRIQUE DOS SANTOS",
			"MIRIAN CAMILA MELENDI",
			"KHAYLANE LORSCHEITER DOS SANTOS" };

	@FXML
	private Button btnGerar;

	@FXML
	private Button btnLimpar;

	@FXML
	private Button btnCopiar;

	@FXML
	private ComboBox<String> cbAvaliacao;

	@FXML
	private ComboBox<String> cbSala;

	@FXML
	private Spinner<Integer> spNumero;

	@FXML
	private TextArea txtResultado;

	private int numero;
	private String[] listaSala = { "1ª A NEM", "1ª A TDS", "3ª C Tancredo" };
	private String[] listaAvaliacao = { "Avaliação 1", "Avaliação 1 - REC", "Avaliação 2", "Avaliação 2 - REC" };
	private String sala, avaliacao;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 50);
		valueFactory.setValue(50);
		spNumero.setValueFactory(valueFactory);
		numero = spNumero.getValue();

		spNumero.valueProperty().addListener(new ChangeListener<Integer>() {

			@Override
			public void changed(ObservableValue<? extends Integer> arg0, Integer arg1, Integer arg2) {

				numero = spNumero.getValue();
				// txtResultado.appendText("#" + numero);
			}
		});

		cbSala.getItems().addAll(listaSala);
		cbSala.setOnAction(this::getSala);
		cbSala.getSelectionModel().select(0);
		sala = cbSala.getValue();

		cbAvaliacao.getItems().addAll(listaAvaliacao);
		cbAvaliacao.setOnAction(this::getAvaliacao);
		cbAvaliacao.getSelectionModel().select(1);
		avaliacao = cbAvaliacao.getValue();
	}

	public void getSala(ActionEvent event) {
		sala = cbSala.getValue();
	}

	public void getAvaliacao(ActionEvent event) {
		avaliacao = cbAvaliacao.getValue();
	}

	public void btnGerarClick() {
		txtResultado.clear();
		String alunos[];

		switch (sala) {
			case "1ª A NEM":
				alunos = listaNem;
				break;
			case "1ª A TDS":
				alunos = listaTds;
				break;
			default:
				alunos = listaTancredo;
		}
		// if (sala.equals("1ª A NEM")) {
		// alunos = listaNem;
		// } else {
		// alunos = listaTds;
		// }

		if (alunos.length < numero) {
			txtResultado.appendText("Não existe esse número na chamada\n");
		} else {
			txtResultado.appendText(sala + '\n');
			txtResultado.appendText(avaliacao.toUpperCase() + '\n');
			txtResultado.appendText(numero + "\t" + alunos[numero - 1] + '\n');
		}

		switch (sala) {
			case "1ª A NEM":
				switch (avaliacao) {
					case "Avaliação 1":
						prova1();
						break;
					case "Avaliação 1 - REC":
						PrimeiroAnoRec rec1 = new PrimeiroAnoRec(numero);
						txtResultado.appendText(rec1.geraValores());
						break;
					default:
						txtResultado.appendText("Não implementado ainda\n");
						break;
				}

			case "1ª A TDS":
				switch (avaliacao) {
					case "Avaliação 1":
						prova1();
						break;
					case "Avaliação 1 - REC":
						PrimeiroAnoRec rec1 = new PrimeiroAnoRec(numero);
						txtResultado.appendText(rec1.geraValores());
						break;

					default:
						txtResultado.appendText("Não implementado ainda\n");
						break;
				}
			case "3ª C Tancredo":
				TerceiroTancredo av1 = new TerceiroTancredo(numero);
				txtResultado.appendText(av1.prova1());
				break;
			default:
				txtResultado.appendText("A sala escolhida é inválida\n");
		}
	}

	public void btnLimparClick() {
		txtResultado.clear();
	}

	public void btnCopiarClick() {
		txtResultado.selectAll();
		txtResultado.copy();
	}

	private void prova1() {
		txtResultado.appendText("Questão 1\n");
		questao1Prova1();
		linha();
		txtResultado.appendText("Questão 3\n");
		questao3Prova1();
		txtResultado.appendText("Questão 4\n");
		questao4Prova1();
	}

	private void questao1Prova1() {
		int t = numero;
		txtResultado.appendText(String.format("C_32 = 4 * 5 + 9 * %d + %d * 7\n", t, t));
		txtResultado.appendText(String.format("C_32 = %d\n", (4 * 5 + 9 * t + t * 7)));
	}

	private void questao3Prova1() {
		int t = numero;
		txtResultado.appendText("Letra A\n");
		txtResultado.appendText(String.format("S_11 = %d + 9\n", t));
		txtResultado.appendText(String.format("S_11 = %d\n", (t + 9)));
		txtResultado.appendText(String.format("S_12 = 2 + 5 + %d\n", t));
		txtResultado.appendText(String.format("S_12 = %d\n", (2 + 5 + t)));
		txtResultado.appendText(String.format("S_21 = 5 + 2 * %d\n", t));
		txtResultado.appendText(String.format("S_21 = %d\n", (5 + 2 * t)));
		txtResultado.appendText(String.format("S_22 = -%d + 1\n", t));
		txtResultado.appendText(String.format("S_22 = %d\n", (-t + 1)));
		txtResultado.appendText("Matriz SOMA\n");
		txtResultado.appendText(String.format("%d  %d\n%d  %d\n", (t + 9), (2 + 5 + t), (5 + 2 * t), (-t + 1)));
		linha();
		txtResultado.appendText("Letra B\n");

		txtResultado.appendText(String.format("D_11 = 5 * %d - 9\n", t));
		txtResultado.appendText(String.format("D_11 = %d\n", 5 * t - 9));
		txtResultado.appendText(String.format("D_12 = 5 * 2 - (5 + %d)\n", t));
		txtResultado.appendText(String.format("D_12 = %d\n", 5 * 2 - (5 + t)));
		txtResultado.appendText(String.format("D_21 = 5 * 5 - 2 * %d\n", t));
		txtResultado.appendText(String.format("D_21 = %d\n", 5 * 5 - 2 * t));
		txtResultado.appendText(String.format("D_22 = 5 * (-%d) - 1\n", t));
		txtResultado.appendText(String.format("D_22 = %d\n", -5 * t - 1));

		txtResultado.appendText("Matriz Obtida\n");
		txtResultado.appendText(
				String.format("%d  %d\n%d  %d\n", (5 * t - 9), (5 * 2 - (5 + t)), (5 * 5 - 2 * t), (-5 * t - 1)));
		linha();
		txtResultado.appendText("Letra C\n");
		txtResultado.appendText("Escalar por Matriz\n");
		txtResultado.appendText(String.format("%d  %d  %d\n%d  %d  %d\n%d  %d  %d\n", (t * 1), (t * 3), (t * 2),
				(t * 2), -t, (t * 5), (t * 2), (t * 3), (t * 4)));
		linha();
		txtResultado.appendText("Letra D\n");
		txtResultado.appendText("Matriz Transposta\n");
		txtResultado.appendText(String.format("%d  %d\n%d  %d\n", 9, (2 * t), (5 + t), 1));
		linha();

	}

	private void questao4Prova1() {
		int M[][] = new int[2][3];
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 3; j++) {
				M[i][j] = numero * (1 + i) - (1 + j) + 10;
				txtResultado.appendText(String.format("M_%d%d = %d * %d - %d +10 = %d\n", (i + 1), (j + 1), numero,
						(i + 1), (j + 1), M[i][j]));
			}
		}
	}

	private void linha() {
		txtResultado.appendText("-----------------\n");
	}
}
